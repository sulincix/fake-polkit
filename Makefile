build: clean
	mkdir build
	# fake polkit library
	$(CC) fakepolkit.c -o build/libfakepolkit.so -shared
	ln -s libfakepolkit.so build/libpolkit-agent-1.so.0
	ln -s libfakepolkit.so build/libpolkit-gobject-1.so.0
	# fake pkexec
	$(CC) pkexec.c -o build/pkexec
	ln -s pkexec build/pkaction
	ln -s pkexec build/pkcheck
	ln -s pkexec build/pkttyagent
	ln -s pkexec build/pk-example-frobnicate

clean:
	rm -rf build
